# Description  
This role install the piwik web analytic server.  

# Usage  
This modes supports the following variables:  

* piwik_database_user: the name of the database user (defaults to piwik)  
* piwik_database_password: the password of the database user (must be specified)  
* piwik_database_name: the name of the database (defaults to piwik)  
* piwiki_database_tables_prefix: prefix to user to create the tables  

# Dependencies  
This module depends on the dledanseur.apache2 and dledanseur.mysql. It therefore install the mysql database if not already done, and configure the database and a user to access it. There is therefore no variable to set the database host name to anything else than localhost.